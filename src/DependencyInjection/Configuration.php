<?php


namespace Esol\DeliveryRestrictionBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('esol_delivery_restriction');

        if (method_exists($treeBuilder, 'getRootNode')) {
            $rootNode = $treeBuilder->getRootNode();
        } else {
            // BC for symfony/config < 4.2
            $rootNode = $treeBuilder->root('esol_delivery_restriction');
        }
        $rootNode->children()
                ->booleanNode('test')->defaultTrue()->end()
                ->end();
        return $treeBuilder;
    }
}