<?php

namespace Esol\DeliveryRestrictionBundle\Repository;

use Esol\DeliveryRestrictionBundle\Entity\CityDeliveryRestriction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CityDeliveryRestriction|null find($id, $lockMode = null, $lockVersion = null)
 * @method CityDeliveryRestriction|null findOneBy(array $criteria, array $orderBy = null)
 * @method CityDeliveryRestriction[]    findAll()
 * @method CityDeliveryRestriction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CityDeliveryRestrictionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CityDeliveryRestriction::class);
    }

    // /**
    //  * @return CityDeliveryRestriction[] Returns an array of CityDeliveryRestriction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CityDeliveryRestriction
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
