<?php

namespace Esol\DeliveryRestrictionBundle\Repository;

use Esol\DeliveryRestrictionBundle\Entity\ProductDeliveryRestriction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductDeliveryRestriction|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductDeliveryRestriction|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductDeliveryRestriction[]    findAll()
 * @method ProductDeliveryRestriction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductDeliveryRestrictionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductDeliveryRestriction::class);
    }

    // /**
    //  * @return ProductDeliveryRestriction[] Returns an array of ProductDeliveryRestriction objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductDeliveryRestriction
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
