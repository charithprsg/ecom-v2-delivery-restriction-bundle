<?php

namespace Esol\DeliveryRestrictionBundle\Entity;

use Esol\DeliveryRestrictionBundle\Repository\CityDeliveryRestrictionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CityDeliveryRestrictionRepository::class)
 */
class CityDeliveryRestriction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }
}
