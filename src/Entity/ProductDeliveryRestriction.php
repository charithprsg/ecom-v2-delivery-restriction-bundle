<?php

namespace Esol\DeliveryRestrictionBundle\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\DeliveryRestriction\Repository\ProductDeliveryRestrictionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=ProductDeliveryRestrictionRepository::class)
 */
class ProductDeliveryRestriction
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }
}
